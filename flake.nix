{

  inputs = {
    naersk.url = "github:nmattia/naersk/master";
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    utils.url = "github:numtide/flake-utils";
    flake-compat = {
      url = github:edolstra/flake-compat;
      flake = false;
    };
  };

  outputs = { self, nixpkgs, utils, naersk, ... }:
    utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };
        naersk-lib = pkgs.callPackage naersk { };
      in
      {
        defaultPackage = naersk-lib.buildPackage {
          src = ./.;
          cargoBuildOptions = x: x ++ [ "-p" "nix-nar-cli" ];
          cargoTestOptions = x: x ++ [ "-p" "nix-nar-cli" ];
          doCheck = true;
          pname = "nix-nar";
        };

        defaultApp = utils.lib.mkApp {
          drv = self.defaultPackage."${system}";
        };

        devShell = with pkgs; mkShell {
          buildInputs = [
            cargo
            cargo-insta
            cargo-nextest
            cargo-outdated
            difftastic
            just
            pre-commit
            rust-analyzer
            rustPackages.clippy
            rustc
            rustfmt
            tokei
          ];
          RUST_SRC_PATH = rustPlatform.rustLibSrc;
        };
      });
}
