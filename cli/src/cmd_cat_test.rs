use crate::cmd_cat::{self, Opts};
use camino::Utf8Path;
use insta::assert_snapshot;

#[test]
fn test_small_file() {
    fn run_cat(path: &str) -> Result<String, anyhow::Error> {
        let mut output: Vec<u8> = vec![];
        let opts = Opts {
            nar: Utf8Path::new("test-data/07-nested-dirs.nar").to_path_buf(),
            path: Utf8Path::new(path).to_path_buf(),
        };
        cmd_cat::run(&mut output, opts)?;
        Ok(std::str::from_utf8(&output)?.to_string())
    }
    assert_snapshot!(
        run_cat("/02-some-dir/small-file").unwrap(),
        @r###"This is a test file.
        "###
    );
}
