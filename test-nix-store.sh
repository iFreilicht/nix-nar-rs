#!/usr/bin/env bash

set -e -o pipefail

NIX_NAR="target/release/nix-nar"

function test-path {
    TARGET="$1"
    IDX="$2"
    NUM_FILES="$3"
    echo -ne "\033[1K"
    echo -ne "\r[${IDX}/${NUM_FILES}] Testing ${TARGET}... "
    if [[ ! -d "${TARGET}" ]]; then
        echo -n "not a directory"
        return 0
    fi
    CHECKSUM1=$(nix nar dump-path ${TARGET} 2> /dev/null | md5sum)
    CHECKSUM2=$(${NIX_NAR} dump-path ${TARGET} | md5sum)
    if [[ "x${CHECKSUM1}" == "x${CHECKSUM2}" ]]; then
        echo -n "Ok"
    else
        echo "FAILED"
        echo 'Checksum with `nix nar` was ' ${CHECKSUM1}
        echo 'Checksum with `nix-nar` was ' ${CHECKSUM2}
        exit 1
    fi
}

cargo build --release --workspace
NUM_FILES=$(ls -1 /nix/store | wc -l)
IDX=0
for d in /nix/store/*; do
    IDX=$(( IDX + 1 ))
    test-path "$d" "${IDX}" "${NUM_FILES}"
done
